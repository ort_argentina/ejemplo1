// src/store/index.js

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex) // Habilita el uso del store desde 'cualquier lado'

export default function () {
  const Store = new Vuex.Store({
    state: { // los datos que quiero 'compartir' en la aplicacion
      count: 0
    },
    mutations: { // son funciones que modifican en estado
      increment: state => state.count++,
      decrement: function (state) {
        state.count--
      },
      changeValue: function (state, valor) {
        state.count = valor
      }
    }
  })

  return Store
}
