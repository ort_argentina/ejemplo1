const expr = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = expr();
app.use(cors());
app.use(bodyParser.json()); // support json encoded bodies

app.listen(5000,
    function () {
        console.log('Escuchando');

    });

/* app.get('/',
    function (req,res) {
        console.log('En el get ');
        res.send('Hola');
    }); */

app.get('/sumar',
    function (req, res) {
        let a = parseInt(req.query.numero1);
        let b = parseInt(req.query.numero2);
        let s = a + b;
        let jsonRespuesta = {
            "numero1": a,
            "numero2": b,
            "resultado": s
        }
        // res.send((+req.query.numero1 + +req.query.numero2).toString());
        // res.send(a + " + " + b + " es igual a " + s);
        res.send(jsonRespuesta);
    });

app.post('/sumar',
    function (req, res) {
        console.log(req.body);

        let a = req.body.palabra1;
        let b = req.body.palabra2;
        let s = a + b;
        res.send(s);
    });

const numeros = [5, 4, 1, 6, 7];
app.get('/abm',
    function (req, res) {
        let retorno = {
            "numeros": numeros,
            "length" : numeros.length
        }
        res.send(retorno);
    });

app.post('/abm',
    function (req, res) {
        let num = req.body.numero;
        numeros.push(num);
        res.send(numeros);
    });

app.delete('/abm',
    function (req, res) {
        let i = 0;
        let encontrado = true;
        let num = req.body.numero;
        while(i < numeros.length && encontrado){
            if(numeros[i] == num){
                numeros.splice(i,1);
                encontrado = false;
            }    
        i++;
        }
        res.send(numeros);
    });

