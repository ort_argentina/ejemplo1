import Router from 'vue-router';
import Cliente from '../components/Cliente';
import Proveedor from '../components/Proveedor';

export default new Router({
    routes: [
        {
            path: '/cliente',
            component: Cliente
        },
        {
            path: '/proveedor',
            component: Proveedor
        }
    ]
})