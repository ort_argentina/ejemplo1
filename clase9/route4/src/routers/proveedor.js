import Router from 'vue-router';
import AltaProveedor from '../components/AltaProveedor';
import ModificacionProveedor from '../components/ModificacionProveedor';

export default new Router({
    routes: [
        {
            path: '/alta',
            component: AltaProveedor
        },
        {
            path: '/modificacion',
            component: ModificacionProveedor
        }
    ]
})