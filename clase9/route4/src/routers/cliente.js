import Router from 'vue-router';
import AltaCliente from '../components/AltaCliente';
import ModificacionCliente from '../components/ModificacionCliente';

export default new Router({
    routes: [
        {
            path: '/cliente/alta',
            component: AltaCliente
        },
        {
            path: '/cliente/modificacion',
            component: ModificacionCliente
        }
    ]
})