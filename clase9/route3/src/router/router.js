import Router from 'vue-router';
import Uno from '../components/Uno';
import Dos from '../components/Dos';

export default new Router({
    routes: [{
        path: '/uno', 
        component: Uno
    }, {
        path: '/dos', 
        component: Dos
    }]
});