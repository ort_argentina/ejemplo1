Instalacion del administrador de VueJs
 
```
 npm install -g @vue/cli
```

Crear un proyecto VueJS:
```
 vue create first_vue
```

usar Git Bash en el visual studio code:
En File -> Preferences -> Settings, seleccionar vista JSON.

```
"terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe",
      "terminal.integrated.shellArgs.windows": [
        "--init-file",
        "C:\\Program Files\\Git\\etc\\profile"
    ]
```

Para ejecutar el proyecto creado, entrar al directorio del proyecto.
Luego:

``` npm run serve ```

Doc oficial:

https://vuejs.org/v2/guide/index.html

VAMOS POR EL ONCLICK. 