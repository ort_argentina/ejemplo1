
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'dos', component: () => import('pages/Pagina2.vue') },
      { path: 'tres', component: () => import('pages/Pagina3.vue') },
      { path: 'cuatro', component: () => import('pages/Pagina4.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
