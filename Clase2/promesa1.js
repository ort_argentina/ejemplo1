function sum (a, b) {
   return new Promise(function (resolve, reject) { 
     setTimeout(function () {                                       // send the response after 1 second
       resolve(a + b);
     }, 1000);
   });
}
var myPromise = sum(10, 5);
myPromise.then(function (result) {
  console.log(" 10 + 5: ", result);
}).catch(function (err) {               // The catch handler is called instead, after another second
  console.error(err);                   // => Please provide two numbers to sum.
});
console.log('esta prometido');
