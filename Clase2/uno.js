// Primer archivo
/* function sumar(a, b){
    return a+b;
} */
// console.log();

/* setTimeout(function (){
    console.log(sumar(4, 5));
}, 2000); */

// ASÍ NO!
// setTimeout(sumar(4, 5), 1000);

console.log("inicio");

var p = new Promise(function(resolve, reject) {
    //codigo asincrónico
    /* setTimeout(function() {
        console.log("Termino");
        resolve();
    }, 2000); */
    // reject("todo mal");
    resolve("todo ok papá");
});

p.then(function(a) {
    console.log(a);

}).catch(function(b) {
    console.log(b);
});

console.log("FIN");