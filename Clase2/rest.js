console.log("Probando rest");
// ejemplo1 GET
/* fetch("https://jsonplaceholder.typicode.com/posts/1")
    .then(function(respuesta) {
        return respuesta.json();
    })
    .then(function(resp) {
        console.log(resp);
    }); */
// ejemplo2 GET
fetch("https://restcountries.eu/rest/v2/all")
    .then(function(respuesta) {
        // console.log(respuesta.json());
        return respuesta.json();
    })
    .then(function(resp) {
        // console.log(resp);
        // Funciona igual que el foreach, pero ahora tenemos los dos ejemplos iuju!
        // for (let index = 0; index < resp.length; index++) {
        //     const element = resp[index];
        //     console.log(element.alpha2Code + " | " + element.name);
        // }
        resp.forEach(element => {
            console.log(element.alpha2Code + " | " + element.name);
        });
    });