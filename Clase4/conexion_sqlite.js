const sqlite3 = require('sqlite3');

//var db = new sqlite3.Database('midatos.sqlite', function(err) {    
var db = new sqlite3.Database('midatos.sqlite', (err) => {    
    if (err) {  //Cuando hay error
        console.log('Error al abrir la base de datos:', err);
    } else {
        // La base se abrio correctamente
        console.log('La base de abrio correctamente');
        crearTabla();
    }
});

// const creaTabla = function() {
const crearTabla = () => {
    console.log('Creando tabla...');
    db.run('CREATE TABLE IF NOT EXISTS persona (' +
           'id INTEGER PRIMARY KEY AUTOINCREMENT, ' + 
           'name TEXT)', (err) => {
               insertarPersona();
           } );
}

//var nombre = '/"; drop database midatos '      //SQL Injection

insertarPersona = () => {
    console.log('Insertando persona...');
    db.run('INSERT INTO persona (name) VALUES (?) ', ['Gabriel']);
}

db.close();