# NodeJS y Bases de Datos

## SQLite: Relaciones
## MongoDB: NoSQL


# Preparacion del entorno:

* Configuracion del proyecto:

```
npm init
npm install sqlite3
```

## Entrar a la base de datos para hacer consultas desde la linea de comandos:
```
./sqlite3.exe midatos.sqlite
```

* .tables   muestra las tablas
* .schema persona   muestra el create de la tabla
* .headers on     Al hacer consultas muestra los titulos de las columnas

```
curl localhost:3000/persona -X POST \
     -d '{ "name":"Gabriel", "age":"11" }' \
     --header "Content-Type: application/json"
```