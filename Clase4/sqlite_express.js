const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const db = require('./basedatos');

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get('/persona', (req, res) => {
    db.all('SELECT * FROM persona', (err, rows) => {
        res.send(rows);
    });
});

app.get('/persona/:personaId', (req, res) => {
    db.all('SELECT * FROM persona WHERE id = ?', [req.params.personaId], (err, rows) => {
        if (rows.length > 0)
            res.send(rows[0]);
        else
            //res.sendStatus(404);
            res.status(404).send('Persona No Existente.');
    })
});

app.post('/persona', (req, res) => {
    db.run('INSERT INTO persona (name, age) VALUES (?, ?) ', 
       [req.body.name, req.body.age]);
    res.status(201).send('Persona dada de alta.');
});

app.delete('/persona', (req, res) => {

});

app.put('/persona', (req, res) => {
    db.run('UPDATE persona SET name = ?, age = ? WHERE id = ? ', 
        [req.body.name, req.body.age, req.body.id]);
    res.status(201).send('Persona modificada.');
});


app.listen(port=3000, () => {
    console.log('Servidor escuchando...');
});